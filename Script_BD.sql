USE [master]
GO
/****** Object:  Database [LiquidarSueldos]    Script Date: 29/09/2020 22:46:46 ******/
CREATE DATABASE [LiquidarSueldos]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LiquidarSueldos', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\LiquidarSueldos.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'LiquidarSueldos_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\LiquidarSueldos_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [LiquidarSueldos] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LiquidarSueldos].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LiquidarSueldos] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET ARITHABORT OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LiquidarSueldos] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LiquidarSueldos] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LiquidarSueldos] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LiquidarSueldos] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LiquidarSueldos] SET  MULTI_USER 
GO
ALTER DATABASE [LiquidarSueldos] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LiquidarSueldos] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LiquidarSueldos] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LiquidarSueldos] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LiquidarSueldos] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [LiquidarSueldos] SET QUERY_STORE = OFF
GO
USE [LiquidarSueldos]
GO
/****** Object:  Table [dbo].[Concepto]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Concepto](
	[ID] [int] NOT NULL,
	[Concepto] [varchar](50) NOT NULL,
	[Porcentaje] [int] NOT NULL,
 CONSTRAINT [PK_Concepto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleado](
	[Legajo] [int] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL,
	[Fecha_Nacimiento] [datetime] NULL,
	[Cuil] [varchar](15) NOT NULL,
 CONSTRAINT [PK_Empleado] PRIMARY KEY CLUSTERED 
(
	[Legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recibo]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recibo](
	[ID_Recibo] [int] NOT NULL,
	[Mes_Año] [nchar](10) NULL,
	[Sueldo_Bruto] [decimal](18, 0) NULL,
	[Sueldo_Neto] [decimal](18, 0) NULL,
	[Total_Descuento] [decimal](18, 0) NULL,
	[Legajo] [int] NOT NULL,
 CONSTRAINT [PK_Recibo] PRIMARY KEY CLUSTERED 
(
	[ID_Recibo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recibo_Concepto]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recibo_Concepto](
	[ID_Recibo] [int] NOT NULL,
	[ID_Concepto] [int] NOT NULL,
 CONSTRAINT [PK_Recibo_Concepto] PRIMARY KEY CLUSTERED 
(
	[ID_Recibo] ASC,
	[ID_Concepto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Recibo_Concepto]  WITH CHECK ADD  CONSTRAINT [FK_Recibo_Concepto_Concepto] FOREIGN KEY([ID_Concepto])
REFERENCES [dbo].[Concepto] ([ID])
GO
ALTER TABLE [dbo].[Recibo_Concepto] CHECK CONSTRAINT [FK_Recibo_Concepto_Concepto]
GO
ALTER TABLE [dbo].[Recibo_Concepto]  WITH CHECK ADD  CONSTRAINT [FK_Recibo_Concepto_Recibo] FOREIGN KEY([ID_Recibo])
REFERENCES [dbo].[Recibo] ([ID_Recibo])
GO
ALTER TABLE [dbo].[Recibo_Concepto] CHECK CONSTRAINT [FK_Recibo_Concepto_Recibo]
GO
/****** Object:  StoredProcedure [dbo].[DeleteConcepto]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteConcepto] 
 
 @Id int
AS
begin

DELETE FROM Recibo_Concepto
WHERE ID_Concepto = @Id

DELETE FROM Concepto
WHERE ID = @Id




end
GO
/****** Object:  StoredProcedure [dbo].[DeleteEmpleado]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteEmpleado] 
 
 @Leg int
AS
begin

DELETE FROM Empleado
WHERE Legajo = @Leg


end
GO
/****** Object:  StoredProcedure [dbo].[DeleteRecibo]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteRecibo] 
 
 @Legajo int, @Fecha varchar(10)
AS
begin
	declare @Id int
		set @Id = (select ID_Recibo from Recibo where Legajo = @Legajo and Mes_Año =@Fecha)


DELETE FROM Recibo_Concepto
WHERE ID_Recibo = @Id

DELETE FROM Recibo
WHERE ID_Recibo = @Id




end
GO
/****** Object:  StoredProcedure [dbo].[InsertConcepto]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertConcepto] 
 
 
 @Con varchar(20), @Porc int
AS
begin



	declare @Id int
	set @Id = (select ISNULL (MAX(id) , 0) +1 From Concepto)

	INSERT INTO Concepto(ID, Concepto,Porcentaje) VALUES (@Id,RTRIM(@Con), @Porc)


end
GO
/****** Object:  StoredProcedure [dbo].[InsertEmpleado]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertEmpleado] 
 
 
 @Nom varchar(20), @Ape varchar(20), @Fecha datetime, @Cuil varchar(15)
AS
begin



	declare @legajo int
	set @legajo = (select ISNULL (MAX(legajo) , 0) +1 From Empleado)

	INSERT INTO Empleado( Legajo,Nombre,Apellido,Fecha_Nacimiento,cuil) VALUES (@legajo, RTRIM(@nom), RTRIM(@Ape),@Fecha,@Cuil)

	declare @id int
	set @id = (select ISNULL (MAX(ID_Recibo) , 0) +1 From Recibo)


end
GO
/****** Object:  StoredProcedure [dbo].[InsertRecibo]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertRecibo] 
 
 
 @Id int, @Fecha varchar(10), @Bruto decimal, @Neto decimal, @Total decimal,@Legajo int
AS
begin



	INSERT INTO Recibo(ID_Recibo, Mes_Año,Sueldo_Bruto,Sueldo_Neto,Total_Descuento,Legajo)
	VALUES (@Id,RTRIM(@Fecha), @Bruto,@Neto,@Total,@Legajo)


end
GO
/****** Object:  StoredProcedure [dbo].[InsertReciboConcepto]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[InsertReciboConcepto] 
 
 
 @Id int, @Concepto int
AS
begin



	INSERT INTO Recibo_Concepto(ID_Recibo, ID_Concepto)
	VALUES (@Id,@Concepto)


end
GO
/****** Object:  StoredProcedure [dbo].[ListarConceptos]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ListarConceptos] 
 

AS
begin
SELECT *
FROM Concepto
end
GO
/****** Object:  StoredProcedure [dbo].[ListarConceptosEmple]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ListarConceptosEmple] 
 
 @Legajo int, @Fecha varchar(10)
AS
begin
  SELECT RC.ID_Concepto, C.Concepto,C.Porcentaje
  FROM Recibo R INNER JOIN Recibo_Concepto RC ON R.ID_Recibo = RC.ID_Recibo INNER JOIN Concepto C ON RC.ID_Concepto = C.ID
  WHERE R.Legajo = @Legajo AND R.Mes_Año =RTRIM(@Fecha)
end
GO
/****** Object:  StoredProcedure [dbo].[ListarEmpleado]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ListarEmpleado] 
 

AS
begin
SELECT *
FROM Empleado
end
GO
/****** Object:  StoredProcedure [dbo].[ListarIDMax]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ListarIDMax] 
 

AS
begin



	select ISNULL (MAX(ID_Recibo) , 0) +1 From Recibo




end
GO
/****** Object:  StoredProcedure [dbo].[ListarRecibo]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ListarRecibo] 
 
 @Legajo int, @Fecha varchar(10)
AS
begin
SELECT R.ID_Recibo,R.Legajo, R.Sueldo_Bruto, R.Sueldo_Neto,R.Total_Descuento,R.Mes_Año, e.Nombre, e.Apellido,e.Cuil
  FROM Recibo R INNER JOIN Empleado E ON r.Legajo = e.Legajo
  WHERE RTRIM(R.Legajo) = @Legajo AND RTRIM(r.Mes_Año) = RTRIM(@Fecha)
end
GO
/****** Object:  StoredProcedure [dbo].[ListarReciboEmple]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ListarReciboEmple] 
 
 @Legajo int
AS
begin
SELECT R.Mes_Año
FROM Recibo R
WHERE R.Legajo = @Legajo
end
GO
/****** Object:  StoredProcedure [dbo].[UpdateConcepto]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateConcepto] 
 
 @Id int, @Con varchar(20), @Porc int
AS
begin

update Concepto
set Concepto = RTRIM(@Con), Porcentaje = @Porc
where ID= @id

end
GO
/****** Object:  StoredProcedure [dbo].[UpdateEmpleado]    Script Date: 29/09/2020 22:46:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateEmpleado] 
 
 @Leg int, @Nom varchar(20), @Ape varchar(20), @Fecha datetime, @Cuil varchar(15)
AS
begin

update Empleado
set Nombre = RTRIM(@Nom), Apellido = RTRIM(@Ape), Fecha_Nacimiento = @Fecha, Cuil = @Cuil
where Legajo = @Leg


end
GO
USE [master]
GO
ALTER DATABASE [LiquidarSueldos] SET  READ_WRITE 
GO
