﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
  public class ReciboConcepto
    {
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string mesAño;

        public string MesAño
        {
            get { return mesAño; }
            set { mesAño = value; }
        }

        private decimal sueldoBruto;

        public decimal SueldoBruto
        {
            get { return sueldoBruto; }
            set { sueldoBruto = value; }
        }

        private decimal sueldoNeto;

        public decimal SueldoNeto
        {
            get { return sueldoNeto; }
            set { sueldoNeto = value; }
        }

        private decimal totalDescuentos;

        public decimal TotalDescuentos
        {
            get { return totalDescuentos; }
            set { totalDescuentos = value; }
        }
        private int numero;
        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        private decimal porcAplicado;

        public decimal PorcAplicado
        {
            get { return porcAplicado; }
            set { porcAplicado = value; }
        }
        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private string cuil;

        public string Cuil
        {
            get { return cuil; }
            set { cuil = value; }
        }

        private DateTime fechaAlta;

        public DateTime FechaAlta
        {
            get { return fechaAlta; }
            set { fechaAlta = value; }
        }
    }
}
