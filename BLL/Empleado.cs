﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
namespace BLL
{
   public class Empleado
    {
        private Acceso acceso = new Acceso();
        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private string cuil;

        public string Cuil
        {
            get { return cuil; }
            set { cuil = value; }
        }

        private DateTime fechaAlta;

        public DateTime FechaAlta
        {
            get { return fechaAlta; }
            set { fechaAlta = value; }
        }      
        public void Insertar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Nom", this.nombre));
            parameters.Add(acceso.CrearParametro("@Ape", this.apellido));
            parameters.Add(acceso.CrearParametro("@Fecha", this.fechaAlta));
            parameters.Add(acceso.CrearParametro("@Cuil", this.cuil));
            acceso.Escribir("InsertEmpleado", parameters);

            acceso.Cerrar();
        }
        public static List<Empleado> Listar()
        {
            List<Empleado> lista = new List<Empleado>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("ListarEmpleado");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Empleado e = new Empleado();
                e.legajo = int.Parse(registro[0].ToString());
                e.nombre = registro[1].ToString();
                e.apellido = registro[2].ToString();
                e.cuil = registro[4].ToString();
                e.fechaAlta = DateTime.Parse(registro[3].ToString());
                lista.Add(e);
            }
            return lista;
        }
        public void Editar()
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Leg", this.legajo));
            parameters.Add(acceso.CrearParametro("@Nom", this.nombre));
            parameters.Add(acceso.CrearParametro("@Ape", this.apellido));
            parameters.Add(acceso.CrearParametro("@Fecha", this.fechaAlta));
            parameters.Add(acceso.CrearParametro("@Cuil", this.cuil));
            acceso.Escribir("UpdateEmpleado", parameters);

            acceso.Cerrar();
        }

        public void Borrar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Leg", this.legajo));

            acceso.Escribir("DeleteEmpleado", parameters);

            acceso.Cerrar();
        }
    }
}
