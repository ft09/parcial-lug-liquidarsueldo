﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
namespace BLL
{
    public class Recibo
    {
        Acceso acceso = new Acceso();
        Concepto concepto = new Concepto();
        Empleado emple = new Empleado();
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string mesAño;

        public string MesAño
        {
            get { return mesAño; }
            set { mesAño = value; }
        }

        private decimal sueldoBruto;

        public decimal SueldoBruto
        {
            get { return sueldoBruto; }
            set { sueldoBruto = value; }
        }

        private decimal sueldoNeto;

        public decimal SueldoNeto
        {
            get { return sueldoNeto; }
            set { sueldoNeto = value; }
        }

        private decimal totalDescuentos;

        public decimal TotalDescuentos
        {
            get { return totalDescuentos; }
            set { totalDescuentos = value; }
        }

        public static List<Recibo> Listar()
        {
            List<Recibo> lista = new List<Recibo>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("ListarConceptos");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Recibo r = new Recibo();
                r.Id = int.Parse(registro[0].ToString());
                r.SueldoBruto = decimal.Parse(registro[2].ToString());
                r.sueldoNeto = decimal.Parse(registro[2].ToString());
                r.TotalDescuentos = decimal.Parse(registro[2].ToString());
                r.MesAño = (registro[2].ToString());
                lista.Add(r);
            }
            return lista;
        }
        public static List<ReciboConcepto> ListarRecibo(int legajo, string fecha)
        {
            List<ReciboConcepto> listaRecibo = new List<ReciboConcepto>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Legajo", legajo));
            parameters.Add(acceso.CrearParametro("@Fecha", fecha));
            DataTable tabla = acceso.Leer("ListarRecibo",parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                ReciboConcepto r = new ReciboConcepto();


                r.Id = int.Parse(registro[0].ToString());
                r.Legajo = int.Parse(registro[1].ToString());
                r.SueldoBruto = decimal.Parse(registro[2].ToString());
                r.SueldoNeto = decimal.Parse(registro[3].ToString());
                r.TotalDescuentos = decimal.Parse(registro[4].ToString());
                r.MesAño = (registro[5].ToString());
                r.Nombre = (registro[6].ToString());
                r.Apellido = (registro[7].ToString());
                r.Cuil = (registro[8].ToString());
                listaRecibo.Add(r);
            }
            return listaRecibo;
        }
        public static List<ReciboConcepto> ListarReciboEmple(int legajo)
        {
            List<ReciboConcepto> listaRecibos = new List<ReciboConcepto>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Legajo", legajo));
            DataTable tabla = acceso.Leer("ListarReciboEmple", parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                ReciboConcepto r = new ReciboConcepto();

                r.MesAño = (registro[0].ToString());

                listaRecibos.Add(r);
            }
            return listaRecibos;
        }
        public static List<ReciboConcepto> ListarConceptoEmple(int legajo, string fecha)
        {
            List<ReciboConcepto> listaConceptoEmple = new List<ReciboConcepto>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Legajo", legajo));
            parameters.Add(acceso.CrearParametro("@Fecha", fecha));
            DataTable tabla = acceso.Leer("ListarConceptosEmple", parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                ReciboConcepto r = new ReciboConcepto();

                r.Numero = int.Parse(registro[0].ToString());
                r.Descripcion = registro[1].ToString();
                r.PorcAplicado = int.Parse(registro[2].ToString());
                listaConceptoEmple.Add(r);
            }
            return listaConceptoEmple;
        }
        public void Insertar(int legajo, List<Concepto> lista)
        {
            acceso.Abrir();
            Id = DevuelveID();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Id", this.Id));
            parameters.Add(acceso.CrearParametro("@Fecha", this.MesAño));
            parameters.Add(acceso.CrearParametro("@Bruto", this.SueldoBruto));
            parameters.Add(acceso.CrearParametro("@Neto", this.SueldoNeto));
            parameters.Add(acceso.CrearParametro("@Total", this.TotalDescuentos));
            parameters.Add(acceso.CrearParametro("@Legajo", legajo));
            acceso.Escribir("InsertRecibo", parameters);

            parameters.Clear();
            foreach (var item in lista)
            {
                parameters.Add(acceso.CrearParametro("@Id", this.Id));
                parameters.Add(acceso.CrearParametro("@Concepto", item.Numero));
                acceso.Escribir("InsertReciboConcepto", parameters);
                parameters.Clear();
            }
            acceso.Cerrar();
        }
        //public void Editar()
        //{

        //    acceso.Abrir();
        //    List<IDbDataParameter> parameters = new List<IDbDataParameter>();

        //    //parameters.Add(acceso.CrearParametro("@Id", this.Numero));
        //    //parameters.Add(acceso.CrearParametro("@Con", this.Descripcion));
        //    //parameters.Add(acceso.CrearParametro("@Porc", this.PorcAplicado));

        //    acceso.Escribir("UpdateConcepto", parameters);

        //    acceso.Cerrar();
        //}

        public void Borrar(int legajo, string fecha)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Legajo", legajo));
            parameters.Add(acceso.CrearParametro("@Fecha", fecha));

            acceso.Escribir("DeleteRecibo", parameters);

            acceso.Cerrar();
        }

        public void Liquidacion(List<Concepto> lista, int legajo)
        {           
          double descuentos =  Convert.ToDouble(lista.Sum(x => x.PorcAplicado));
           double aa = (descuentos / 100);
          TotalDescuentos = Convert.ToDecimal(aa) * SueldoBruto;
          SueldoNeto = SueldoBruto - TotalDescuentos;
            Insertar(legajo, lista);
        }

       public int DevuelveID()
        {
            Recibo r = new Recibo();
            DataTable tabla = acceso.Leer("ListarIDMax");
            foreach (DataRow registro in tabla.Rows)
            {               
                r.Id = int.Parse(registro[0].ToString());
            }
            return r.Id;
        }
    }
}
