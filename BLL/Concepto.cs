﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using DAL;
namespace BLL
{
    public class Concepto
    {
        public Concepto(int id, string des, int porc)
        {
            Numero = id;
            Descripcion = des;
            PorcAplicado = porc;
        }
        public Concepto(){}
   
        Acceso acceso = new Acceso();
        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        private decimal porcAplicado;

        public decimal PorcAplicado
        {
            get { return porcAplicado; }
            set { porcAplicado = value; }
        }


        public Boolean Exist { get; set; }
        public List<Concepto> listaConceptos = new List<Concepto>();
        public List<Concepto> listaCopia { get; set; }

        public List<Concepto> AgregarLista(int id, List<Concepto> listaCopia)
        {
            foreach (var item in listaCopia)
            {
                if (item.Numero == id)
                {
                    item.Exist = false;
                    listaConceptos.Add(item);
                }
            }
            return listaConceptos;
        }
        public List<Concepto> QuitarLista(int id, List<Concepto> asd)
        {
            foreach (var item in asd)
            {
                if (item.Numero == id)
                {
                    item.Exist = true;
                    listaConceptos.Remove(item);
                }
            }
            return listaConceptos;
        }
        public static List<Concepto> Listar()
        {
            List<Concepto> lista = new List<Concepto>();
            //List<Concepto> listaCopia = new List<Concepto>();


            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("ListarConceptos");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Concepto c = new Concepto();
                c.Numero = int.Parse(registro[0].ToString());
                c.Descripcion= registro[1].ToString();
                c.PorcAplicado = int.Parse(registro[2].ToString());
                c.Exist = true;
                lista.Add(c);
                //listaCopia.Add(c);   
            }
            return lista;
        }
        public static List<Concepto> ListarFiltrado()
        {
            //List<Concepto> lista = new List<Concepto>();
            List<Concepto> listaCopia = new List<Concepto>();


            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("ListarConceptos");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Concepto c = new Concepto();
                c.Numero = int.Parse(registro[0].ToString());
                c.Descripcion = registro[1].ToString();
                c.PorcAplicado = int.Parse(registro[2].ToString());
                c.Exist = true;
                listaCopia.Add(c);
            }
            return listaCopia;
        }
        public void Insertar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Con", this.Descripcion));
            parameters.Add(acceso.CrearParametro("@Porc", this.PorcAplicado));
            acceso.Escribir("InsertConcepto", parameters);

            acceso.Cerrar();
        }
        public void Editar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Id", this.Numero));
            parameters.Add(acceso.CrearParametro("@Con", this.Descripcion));
            parameters.Add(acceso.CrearParametro("@Porc", this.PorcAplicado));

            acceso.Escribir("UpdateConcepto", parameters);

            acceso.Cerrar();
        }

        public void Borrar()
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Id", this.Numero));

            acceso.Escribir("DeleteConcepto", parameters);

            acceso.Cerrar();
        }


    }
}
