﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
namespace LiquidarSueldo
{
    public partial class FormEmpleado : Form
    {
        Empleado emple = new Empleado();
        public FormEmpleado()
        {
            InitializeComponent();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (!string.IsNullOrEmpty(txtNombre.Text) & !string.IsNullOrEmpty(txtApellido.Text) & txtCuil.MaskFull )
                {
                    emple.Nombre = txtNombre.Text;
                    emple.Apellido = txtApellido.Text;
                    emple.FechaAlta = dtpFecha.Value.Date;
                    emple.Cuil = txtCuil.Text;

                    emple.Insertar();
                    Actualizar();
                    Limpiar();

                }
                else
                {
                    MessageBox.Show("Esta vacío algun campo");
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }       

        }

        public void Actualizar()
        {
            dgvEmpleado.DataSource = null;
            dgvEmpleado.DataSource = Empleado.Listar();
        }
        private void FormEmpleado_Load(object sender, EventArgs e)
        {        
            Actualizar();
        }

        private void dgvEmpleado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtLegajo.Text = dgvEmpleado.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNombre.Text = dgvEmpleado.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApellido.Text = dgvEmpleado.Rows[e.RowIndex].Cells[2].Value.ToString();
            dtpFecha.Value = Convert.ToDateTime( dgvEmpleado.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtCuil.Text = dgvEmpleado.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void btnModif_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtNombre.Text) & !string.IsNullOrEmpty(txtApellido.Text) & txtCuil.MaskFull)
                {
                    emple.Legajo = int.Parse(txtLegajo.Text);
                    emple.Nombre = txtNombre.Text;
                    emple.Apellido = txtApellido.Text;
                    emple.FechaAlta = dtpFecha.Value.Date;
                    emple.Cuil = txtCuil.Text;

                    emple.Editar();
                    Actualizar();
                    Limpiar();
                }
                else
                {
                    MessageBox.Show("Esta vacío algun campo");
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        private void Limpiar()
        {
            txtLegajo.Clear();
            txtNombre.Clear();
            txtApellido.Clear();
            txtCuil.Clear();
        }
        private void btnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtNombre.Text) & !string.IsNullOrEmpty(txtApellido.Text) & txtCuil.MaskFull)
                {
                    emple.Legajo = Convert.ToInt32(txtLegajo.Text);
                    emple.Borrar();
                    Actualizar();
                    Limpiar();
                }
                else
                {
                    MessageBox.Show("Esta vacío algun campo");
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
