﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
namespace LiquidarSueldo
{
    public partial class FormRecibo : Form
    {
        Empleado empleado = new Empleado();
        Recibo recibo = new Recibo();
        int legajo;
        public FormRecibo()
        {
            InitializeComponent();
        }

        private void FormRecibo_Load(object sender, EventArgs e)
        {
            dgvEmpleados.DataSource = null;
            dgvEmpleados.DataSource = Empleado.Listar();
           
        }

        private void dgvEmpleados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            listBox1.Items.Clear();
            comboMes.Items.Clear();
            var EmpleadoSeleccionado = (Empleado)(this.dgvEmpleados.SelectedRows[0].DataBoundItem);
           
            CargarComboFecha(Recibo.ListarReciboEmple(EmpleadoSeleccionado.Legajo));
            legajo = EmpleadoSeleccionado.Legajo;
           
        }
        private void CargarComboFecha(List<ReciboConcepto> lista)
        {
            foreach (var item in lista)
            {
                comboMes.Items.Add(item.MesAño);
            }
        }
        private void AgregarRecibo(List<ReciboConcepto> lista)
        {
            foreach (var item in lista)
            {
                listBox1.Items.Add("NOMBRE: " + item.Nombre.PadRight(20) + "APELLIDO: " + item.Apellido.PadRight(20) + "CUIL: " + item.Cuil);
                listBox1.Items.Add(Environment.NewLine);
                listBox1.Items.Add("ID RECIBO: " + item.Id.ToString().PadRight(20) + " LEGAJO: " + item.Legajo.ToString().PadRight(20) + "MES/AÑO: " + item.MesAño);
                listBox1.Items.Add(Environment.NewLine);
            }
            AgregarConcepto(Recibo.ListarConceptoEmple(legajo,comboMes.Text));
            foreach (var item in lista)
            {
                listBox1.Items.Add(Environment.NewLine);
                listBox1.Items.Add("SUELDO BRUTO:".PadRight(87) + "$ " + item.SueldoBruto);
                listBox1.Items.Add(Environment.NewLine);
                listBox1.Items.Add("TOTAL DE DESCUENTOS:".PadRight(79) + "$ " + item.TotalDescuentos);
                listBox1.Items.Add(Environment.NewLine);
                listBox1.Items.Add("SUELDO NETO:".PadRight(89) + "$ " + item.SueldoNeto);
                listBox1.Items.Add(Environment.NewLine);
            }
        }
        private void AgregarConcepto(List<ReciboConcepto> lista)
        {
            listBox1.Items.Add("CONCEPTOS: ");
            foreach (var item in lista)
            {
                listBox1.Items.Add(Environment.NewLine);
                listBox1.Items.Add("CODIGO: " + item.Numero.ToString().PadRight(25) + "DESCRIPCIÓN: " + item.Descripcion.PadRight(30) + "% " + item.PorcAplicado + Environment.NewLine);

            }
        }
        private void dgvEmpleados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboMes.SelectedItem != null)
                {
                    
                    listBox1.Items.Clear();
                    AgregarRecibo(Recibo.ListarRecibo(legajo, comboMes.Text.Trim()));
                }
                else
                {
                    MessageBox.Show("No selecciono el mes o falta agregar el empleado ");
                }
            }
            catch (Exception)
            {

            }

        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboMes.SelectedItem != null)
                {
                    recibo.Borrar(legajo, comboMes.Text.Trim());
                    listBox1.Items.Clear();
                    comboMes.Items.Clear();
                    comboMes.Text = " ";
                }
                else
                {
                    MessageBox.Show("Debe seleccionar un Mes/Año del recibo");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show( ex.Message);
            }

        }
    }
}
