﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
namespace LiquidarSueldo
{
    public partial class FormConcepto : Form
    {
        Concepto co = new Concepto();
        public FormConcepto()
        {
            InitializeComponent();
        }

        private void FormConcepto_Load(object sender, EventArgs e)
        {
            Actualizar();
        }
        public void Limpiar()
        {
            txtConcepto.Text = string.Empty;
            txtNumero.Text = string.Empty;
            txtPorcentaje.Text = string.Empty;
        }
        public void Actualizar()
        {
            dgvConceptos.DataSource = null;
            dgvConceptos.DataSource = Concepto.Listar();
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtConcepto.Text) & !string.IsNullOrEmpty(txtPorcentaje.Text))
                {
                    co.Descripcion = txtConcepto.Text;
                    co.PorcAplicado = decimal.Parse(txtPorcentaje.Text);

                    co.Insertar();
                    Actualizar();
                    Limpiar();
                }
                else
                {
                    MessageBox.Show("Los campos estan vacios");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnModif_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtConcepto.Text) & !string.IsNullOrEmpty(txtPorcentaje.Text))
                {
                    co.Numero = int.Parse(txtNumero.Text);
                    co.Descripcion = txtConcepto.Text;
                    co.PorcAplicado = int.Parse(txtPorcentaje.Text);
                    co.Editar();
                    Actualizar();
                    Limpiar();
                }
                else
                {
                    MessageBox.Show("Los campos estan vacios");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void dgvConceptos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtNumero.Text = dgvConceptos.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtConcepto.Text = dgvConceptos.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtPorcentaje.Text = dgvConceptos.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtConcepto.Text) & !string.IsNullOrEmpty(txtPorcentaje.Text))
                {
                    co.Numero = int.Parse(txtNumero.Text);
                    co.Borrar();
                    Actualizar();
                    Limpiar();
                }
                else
                {
                    MessageBox.Show("Los campos estan vacios");
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
