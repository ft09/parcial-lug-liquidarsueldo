﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace LiquidarSueldo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void empleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormEmpleado formEmple = new FormEmpleado();
            formEmple.Show();
        }

        private void conceptosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormConcepto formConcep = new FormConcepto();
            formConcep.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void liquidarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormLiquidar formLiquidar = new FormLiquidar();
            formLiquidar.Show();
        }

        private void recibosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormRecibo formRecibo = new FormRecibo();
            formRecibo.Show();
        }
    }
}
