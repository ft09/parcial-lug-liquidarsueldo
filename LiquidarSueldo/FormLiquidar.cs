﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace LiquidarSueldo
{
    public partial class FormLiquidar : Form
    {
        Recibo recibo = new Recibo();
        Concepto concepto = new Concepto();
        List<Concepto> listaConceptos = new List<Concepto>();
        public FormLiquidar()
        {
            InitializeComponent();
        }

        private void FormLiquidar_Load(object sender, EventArgs e)
        {
            Cargar();
        }

        public void Cargar()
        {
            listaConceptos = Concepto.ListarFiltrado();
            dgvConceptos.DataSource = null;
            dgvConceptos.DataSource = listaConceptos;
            dgvEmpleados.DataSource = null;
            dgvEmpleados.DataSource = Empleado.Listar();

        }
        public void Limpiar()
        {
            txtConcepto.Text = string.Empty;
            txtPorcentaje.Text = string.Empty;
            txtNumero.Text = string.Empty;
        }
        public void Actualizar()
        {
            dgvConceptos.DataSource = null;
            dgvConceptos.DataSource = listaConceptos.Where(x => x.Exist).ToList();
            dgvConceptosCargados.DataSource = listaConceptos.Where(x => !x.Exist).ToList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var asd = new Concepto(int.Parse(txtNumero.Text), txtConcepto.Text, int.Parse(txtPorcentaje.Text));

                dgvConceptosCargados.DataSource = null;
                dgvConceptosCargados.DataSource = concepto.AgregarLista(int.Parse(txtNumero.Text), listaConceptos);

                Actualizar();
                Limpiar();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Debe Seleccionar un concepto");
            }
            
        }

        private void dgvConceptos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtNumero.Text = dgvConceptos.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtConcepto.Text = dgvConceptos.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtPorcentaje.Text = dgvConceptos.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

        private void dgvEmpleados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtLegajo.Text = dgvEmpleados.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNombre.Text = dgvEmpleados.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApellido.Text = dgvEmpleados.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtCuil.Text = dgvEmpleados.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void btnLiquidar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtLegajo.Text != string.Empty & txtSueldoBruto.Text !=string.Empty)
                {
                    listBox1.Items.Clear();
                    recibo.SueldoBruto = decimal.Parse(txtSueldoBruto.Text);
                    recibo.MesAño = Convert.ToString(dtpFecha.Value.Month) + "-" + dtpFecha.Value.Year;
                    recibo.Liquidacion(concepto.listaConceptos.ToList(), int.Parse(txtLegajo.Text));
                   
                    AgregarRecibo(Recibo.ListarRecibo(int.Parse(txtLegajo.Text), recibo.MesAño.Trim()));
                 
                }
                else
                {
                    MessageBox.Show("El campo sueldo bruto esta vacio o falta seleccionar un Empleado");
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        private void AgregarRecibo(List<ReciboConcepto> lista)
        {
            foreach (var item in lista)
            {
                listBox1.Items.Add("NOMBRE: " + item.Nombre.PadRight(20) +  "APELLIDO: " + item.Apellido.PadRight(20) + "CUIL: " + item.Cuil );
                listBox1.Items.Add(Environment.NewLine);
                listBox1.Items.Add("ID RECIBO: " +item.Id.ToString().PadRight(20) + " LEGAJO: " + item.Legajo.ToString().PadRight(20) + "MES/AÑO: " + item.MesAño);
                listBox1.Items.Add(Environment.NewLine);
            }
            AgregarConcepto(Recibo.ListarConceptoEmple(int.Parse(txtLegajo.Text),dtpFecha.Value.ToString().Trim()));
            foreach (var item in lista)
            {
                listBox1.Items.Add(Environment.NewLine);
                listBox1.Items.Add("SUELDO BRUTO:".PadRight(87) + "$ "+ item.SueldoBruto);
                listBox1.Items.Add(Environment.NewLine);
                listBox1.Items.Add("TOTAL DE DESCUENTOS:".PadRight(79) +"$ " + item.TotalDescuentos);
                listBox1.Items.Add(Environment.NewLine);
                listBox1.Items.Add("SUELDO NETO:".PadRight(89) + "$ "+ item.SueldoNeto);      
                listBox1.Items.Add(Environment.NewLine);
            }
        }
        private void AgregarConcepto(List<ReciboConcepto> lista)
        {
            listBox1.Items.Add("CONCEPTOS: ");
            foreach (var item in lista)
            {
                listBox1.Items.Add(Environment.NewLine);
                listBox1.Items.Add("CODIGO: "+ item.Numero.ToString().PadRight(25) + "DESCRIPCIÓN: " + item.Descripcion.PadRight(30) + "% "+ item.PorcAplicado + Environment.NewLine);
                
            }
        }


        private void btnQuitar_Click(object sender, EventArgs e)
        {
            try
            {
                var asd = new Concepto(int.Parse(txtNumero.Text), txtConcepto.Text, int.Parse(txtPorcentaje.Text));

                dgvConceptosCargados.DataSource = null;
                dgvConceptosCargados.DataSource = concepto.QuitarLista(int.Parse(txtNumero.Text), listaConceptos);
                Actualizar();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Debe Seleccionar un concepto");
            }

        }

        private void dgvConceptosCargados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtNumero.Text = dgvConceptosCargados.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtConcepto.Text = dgvConceptosCargados.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtPorcentaje.Text = dgvConceptosCargados.Rows[e.RowIndex].Cells[2].Value.ToString();
        }
    }
}
